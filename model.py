import torch
import torch.nn as nn
import torch.nn.functional as F

from utils.util import *
from utils.parse_config import *


class Darknet(nn.Module):
    def __init__(self, config_path, image_size):
        super(Darknet, self).__init__()
        self.model_defs = model_parse(config_path)
        self.hypterparam, self.model_list = create_model(self.model_defs)
        self.yolo_layers = [layer[0] for layer in self.model_list if hasattr(layer[0], 'metrics')]
        self.image_size = image_size

    def forward(self, x, target=None):
        image_size = x.size(0)
        loss = 0
        layer_outputs, yolo_outputs = [], []
        for i, (model_def, model) in enumerate(zip(self.model_defs, self.model_list)):
            if model_def['type'] in ['convolutional', 'upsample']:
                x = model(x)
            elif model_def['type'] == 'route':
                x = torch.cat([layer_outputs[int(i)] for i in model_def['layers'].split(',')])
            elif model_def['type'] == 'shortcut':
                from_layer = int(model_def['from'])
                print(layer_outputs[from_layer].shape)
                print(layer_outputs[-1].shape)
                x = layer_outputs[-1] + layer_outputs[from_layer]
            elif model_def['type'] == 'yolo':
                x, layer_loss = model(x, target, image_size)
                loss += layer_loss
                yolo_outputs.append(x)
            print(x.shape)
            layer_outputs.append(x)

        yolo_outputs = to_cpu(torch.cat(yolo_outputs, 1))
        if target:
            return loss, yolo_outputs
        return yolo_outputs

    def load_darknet_weight(self):

        pass

    def save_darknet_weight(self):
        pass


def create_model(model_dfs):
    hyperparam = model_dfs.pop(0)
    model_list = nn.ModuleList()
    outfilter = [int(hyperparam['channels'])]

    for model_i, model_def in enumerate(model_dfs):
        model = nn.Sequential()
        if model_def['type'] == 'convolutional':
            bn = int(model_def['batch_normalize'])
            filters = int(model_def['filters'])
            kernel_size = int(model_def['size'])
            stride = int(model_def['stride'])
            padding = kernel_size // 2
            model.add_module(f'conv_{model_i}',
                             nn.Conv2d(in_channels=outfilter[-1],
                                       out_channels=filters,
                                       kernel_size=kernel_size, stride=stride,
                                       padding=padding, bias=not bn))

            if bn:
                model.add_module(f'batch_norm_{model_i}',
                                 nn.BatchNorm2d(filters, momentum=0.9, eps=1e-5))
            if model_def['activation'] == 'leaky':
                model.add_module(f'leaky_{model_i}',
                                 nn.LeakyReLU(0.1))
        elif model_def['type'] == 'upsample':
            stride = int(model_def['stride'])
            model.add_module(f'upsample_{model_i}', Upsample(stride))

        elif model_def['type'] == 'route':
            layers = [int(x) for x in model_def['layers'].split(',')]
            filters = sum([outfilter[1:][x] for x in layers])
            model.add_module(f'route_{model_i}', EmptyLayer())

        elif model_def['type'] == 'shortcut':
            from_layer = int(model_def['from'])
            filters = outfilter[1:][from_layer]
            model.add_module(f'shortcut_{model_i}', EmptyLayer())

        elif model_def['type'] == 'yolo':
            anchor_index = [int(x) for x in model_def['mask'].split(',')]
            anchors = [int(x) for x in model_def['anchors'].split(',')]
            anchors = [(anchors[i], anchors[i + 1]) for i in range(0, len(anchors), 2)]
            anchors = [anchors[anchor_idx] for anchor_idx in anchor_index]
            classes = int(model_def['classes'])
            image_size = int(hyperparam['width'])
            yolo_layer = YoloLayer(classes, image_size, anchors)
            model.add_module(f'yolo_{model_i}', yolo_layer)

        model_list.append(model)
        outfilter.append(filters)

    return hyperparam, model_list


class Upsample(nn.Module):
    def __init__(self, scale_factor, mode='nearest'):
        super(Upsample, self).__init__()
        self.scale_factor = scale_factor
        self.mode = mode

    def forward(self, x):
        x = F.interpolate(x, scale_factor=self.scale_factor, mode=self.mode)
        return x


class EmptyLayer(nn.Module):
    def __init__(self):
        super(EmptyLayer, self).__init__()


class YoloLayer(nn.Module):
    def __init__(self, classes, image_size, anchors):
        super(YoloLayer, self).__init__()
        self.classes = classes
        self.image_size = image_size
        self.anchors = anchors

        self.mes_loss = nn.MSELoss()
        self.bce_loss = nn.BCELoss()
        self.metrics = {}

        self.anchor_num = len(self.anchors)
        self.ignore_thres = 0.7
        self.obj_scale = 1
        self.not_obj_scale = 100
        self.grid_size = 0

    def farward(self, x, target=None, image_dim=None):
        FloatTensor = torch.cuda.FloatTensor if x.is_cuda else torch.FloatTensor

        self.image_size = image_dim
        num_batch = x.size(0)
        grid_size = x.size(2)
        predict = (
            x.view(num_batch, self.anchor_num, (self.classes + 5), grid_size, grid_size)
                .permute(0, 1, 3, 4, 2).contiguous()
        )

        x = torch.sigmoid(predict[..., 0])  # center_x
        y = torch.sigmoid(predict[..., 1])  # center_y
        w = predict[..., 2]  # width
        h = predict[..., 3]  # height
        pred_conf = torch.sigmoid(predict[..., 4])  # predict_conf
        pred_cls = torch.sigmoid(predict[..., 5:])  # predict_cls

        if grid_size != self.grid_size:
            self.compute_grid_offsets(grid_size, x.is_cuda)

        pred_boxes = FloatTensor(predict[..., :4].shape)
        pred_boxes[..., 0] = x.data + self.grid_x
        pred_boxes[..., 1] = y.data + self.grid_y
        pred_boxes[..., 2] = torch.exp(w.data) * self.anchor_w
        pred_boxes[..., 3] = torch.exp(h.data) * self.anchor_h

        output = torch.cat(
            (
                pred_boxes.view(num_batch, -1, 4) * self.stride,
                pred_conf.view(num_batch, -1, 1),
                pred_cls.view(num_batch, -1, self.classes)
            ),
            -1
        )
        if target is None:
            return output, 0

        iou_scores, class_mask, obj_mask, noobj_mask, tx, ty, tw, th, tcls, tconf = build_targets(
            pred_boxes=pred_boxes,
            pred_cls=pred_cls,
            target=target,
            anchors=self.anchors,
            ignore_thres=self.ignore_thres
        )

        loss_x = self.mes_loss(x[obj_mask], tx[obj_mask])
        loss_y = self.mes_loss(y[obj_mask], ty[obj_mask])
        loss_w = self.mes_loss(w[obj_mask], tw[obj_mask])
        loss_h = self.mes_loss(h[obj_mask], th[obj_mask])

        loss_obj_conf = self.bce_loss(pred_conf[obj_mask], tconf[obj_mask])
        loss_noboj_conf = self.bce_loss(pred_conf[noobj_mask], tconf[noobj_mask])
        loss_conf = self.obj_scale * loss_obj_conf + self.not_obj_scale * loss_noboj_conf

        loss_cls = self.bce_loss(pred_cls[obj_mask], tcls[obj_mask])
        loss_total = loss_x + loss_y +  loss_w + loss_h + loss_conf + loss_cls

        # Metrics
        cls_acc = 100 * class_mask[obj_mask].mean()
        conf_obj = pred_conf[obj_mask].mean()
        conf_nobj = pred_conf[noobj_mask].mean()
        conf50 = (pred_conf > 0.5).mean()
        iou50 = (iou_scores > 0.5).mean()
        iou75 = (iou_scores > 0.75).mean()

        detect_mask = conf50 * class_mask * tconf
        precision = torch.sum(iou50 * detect_mask) / (conf50.sum() + 1e-16)
        recall50 = torch.sum(iou50 * detect_mask) / (obj_mask.sum() + 1e-16)
        recall75 = torch.sum(iou75 * detect_mask) / (obj_mask.sum() + 1e-16)

        self.metrics = {
            'loss': to_cpu(loss_total).item(),
            'x': to_cpu(loss_x).item(),
            'y': to_cpu(loss_y).item(),
            'w': to_cpu(loss_w).item(),
            'h': to_cpu(loss_h).item(),
            'conf': to_cpu(loss_conf).item(),
            'cls': to_cpu(loss_cls).item(),
            'cls_acc': to_cpu(cls_acc).item(),
            'precision': to_cpu(precision).item(),
            'recall50': to_cpu(recall50).item(),
            'recall75': to_cpu(recall75).item(),
            'conf_obj': to_cpu(conf_obj).item(),
            'conf_no_obj': to_cpu(conf_nobj).item(),
            'grid_size': grid_size
        }
        return output, loss_total


    def compute_grid_offsets(self, grid_size, is_cuda):
        FloatTensor = torch.cuda.FloatTensor if is_cuda else torch.FloatTensor

        self.grid_size = grid_size
        gs = self.grid_size
        self.stride = self.image_size / self.grid_size
        self.grid_x = torch.arange(gs).repeat(gs, 1).view([1, 1, gs, gs]).type(FloatTensor)
        self.grid_y = torch.arange(gs).repeat(gs, 1).t().view([1, 1, gs, gs]).type(FloatTensor)
        self.scale_anchors = FloatTensor([(a_w / self.stride, a_h / self.stride) \
                                          for a_w, a_h in self.anchors])
        self.anchor_w = self.scaled_anchors[:, 0].view((1, self.anchor_num, 1, 1))
        self.anchor_h = self.scaled_anchors[:, 1].view((1, self.anchor_num, 1, 1))


if __name__ == '__main__':
    model_defs = model_parse('config/yolov3.cfg')
    hyperparam, model_list = create_model(model_defs)
