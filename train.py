import argparse
import time
import datetime

import torch
from torch.utils.data import DataLoader
from torch.autograd import Variable
from tensorboardX import SummaryWriter
from terminaltables import AsciiTable

from utils.datasets import *
from utils.parse_config import *
from utils.util import *
from model import Darknet


def train(opt):
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    data = data_parse(opt.data_dir)
    train_path, valid_path = data['train'], data['valid']
    classes = load_classes('data/coco.names')

    print(train_path)

    logger = SummaryWriter(logdir='log')
    
    dataset = ListDataset(train_path, opt.image_size, multiscale=opt.multiscale)
    dataloader = DataLoader(dataset, batch_size=opt.batch_size, shuffle=True, num_workers=opt.num_workers,
                            pin_memory=True, collate_fn=dataset.collate_fn)

    print('dataloader_length', len(dataloader))

    model = Darknet(opt.model_def, opt.image_size)
    model.apply(weight_init)


    input = Variable(torch.randn((32, 3, 416, 416)))
    output = model(input)
    print(output.shape)
    logger.add_graph(model, (input,))
    logger.close()

    return
    # print(model.parameters())

    optimizer = torch.optim.Adam(model.parameters())
    metrics = [
        "grid_size",
        "loss",
        "x",
        "y",
        "w",
        "h",
        "conf",
        "cls",
        "cls_acc",
        "recall50",
        "recall75",
        "precision",
        "conf_obj",
        "conf_noobj",
    ]

    for epoch in range(opt.epoch):
        model.train()
        start_time = time.time()

        for batch_i, (_, images, targets) in enumerate(dataloader):
            batch_bone = epoch * len(dataloader) + batch_i

            images = Variable(images).to(device)
            targets = Variable(targets).to(device)

            loss, output = model(images, targets)
            loss.backward()

            if batch_bone % opt.gradient_accumulations == 0:
                optimizer.step()
                optimizer.zero_grad()

            log_str = '\n-------[epoch %d/%d, batch %d/%d]--------\n' \
                      % (epoch, opt.epoch, batch_i, len(dataloader))
            metrics_table = [['Metrics'], *[f'yolo_layer{i}' for i in range(len(model.yolo_layers))]]

            for i, metric in enumerate(metrics):
                formats = {m: "%6.f" for m in metric}
                formats['grid_size'] = '%2d'
                formats['cls_acc'] = '%.2f'
                row_metrics = [formats[metric] % yolo.metrics.get(metric) for yolo in model.yolo_layers]
                metrics_table += [[metric, *row_metrics]]

                tensorboard_log = {}
                for j, yolo in enumerate(model.yolo_layers):
                    for name, metric in yolo.metrics.items():
                        if name != 'grid_size':
                            tensorboard_log[f'{name}_{j + 1}'] = metric
                tensorboard_log['loss'] = loss.item()
                logger.add_scalars('metrics', tensorboard_log, batch_bone)

            log_str += AsciiTable(metrics_table).table
            log_str += f'\nTotal Loss: {loss.item()}'

            epoch_batch_left = len(dataloader) - (batch_i + 1)
            time_left = datetime.timedelta(seconds=epoch_batch_left * (time.time() - start_time) / (batch_i + 1))
            log_str += f'\n ----- ETA  {time_left}'
            print(log_str)

            if batch_i % opt.evaluation_interval:
                print('--------------Eval Model-------------')
                pass


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--epoch', type=int, default=10)
    parser.add_argument('--batch_size', type=int, default=32)
    parser.add_argument('--gradient_accumulations', type=int, default=2)
    parser.add_argument('--model_def', type=str, default="config/yolov3.cfg")
    parser.add_argument('--data_dir', type=str, default="data/coco.data")
    parser.add_argument('--n_cpu', type=int, default=4)
    parser.add_argument('--num_workers', type=int, default=4)
    parser.add_argument('--image_size', type=int, default=416)
    parser.add_argument('--checkpoint_interval', type=int, default=10)
    parser.add_argument('--evaluation_interval', type=int, default=10)
    parser.add_argument('--multiscale', default=True)

    opt = parser.parse_args()
    print(opt)
    return opt


if __name__ == '__main__':
    train(get_parser())
