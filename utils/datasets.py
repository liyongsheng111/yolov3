import math, os
import glob, random
import numpy as np
import torch
import torch.nn.functional as F

from PIL import Image
from torch.utils.data import Dataset
from torchvision.transforms import transforms


class ImageFloder(Dataset):
    def __init__(self, path, image_size=416):
        self.path_list = sorted(glob.glob("%s/*.*" % path))
        self.image_size = image_size

    def __getitem__(self, index):
        path = self.path_list[index]
        image = transforms.ToTensor()(Image.open(path))
        image = padding(image, 0)
        image = resize(image, self.image_size)
        return path, image

    def __len__(self):
        return len(self.path_list)


class ListDataset(Dataset):
    def __init__(self, path, image_size, argument=True, multiscale=True, normalized_label=True):
        with open(path, 'r') as file:
            self.image_file = file.readlines()
        self.image_file = [line[:-1] for line in self.image_file if line.endswith('\n')]
        self.label_file = [
            path.replace('images', 'labels').replace('.png', '.txt').replace('.jpg', '.txt')
            for path in self.image_file
        ]
        
        self.image_size = image_size
        self.argument = argument
        self.multiscale = multiscale
        self.normalized_label = normalized_label

        self.batch_idx = 0
        self.min_size = self.image_size - 3 * 32
        self.max_size = self.image_size + 3 * 32

    def __getitem__(self, index):
        # image
        image_path = self.image_file[index]
        image = transforms.ToTensor()(Image.open(image_path).convert("RGB"))
        if len(image.shape) != 3:
            image = image.unsequence(0)
            image = image.expand((3, image[1:]))

        _, h, w = image.shape
        h_factor, w_factor = (w, h) if self.normalized_label else (1, 1)
        image, pad_tuple = padding(image, 0)
        _, pad_h, pad_w = image.shape

        # label
        label_path = self.label_file[index]
        target = None
        if os.path.exists(label_path):
            # [*, center_x, center_y, width, heigth]
            #  | ----------x
            #  |
            #  |
            #  y
            boxes = torch.from_numpy(np.loadtxt(label_path).reshape(-1, 5))
            x1 = w_factor * (boxes[:, 1] - boxes[:, 3] / 2)
            x2 = w_factor * (boxes[:, 1] + boxes[:, 3] / 2)
            y1 = h_factor * (boxes[:, 2] - boxes[:, 4] / 2)
            y2 = h_factor * (boxes[:, 2] + boxes[:, 4] / 2)

            x1 += pad_tuple[0]
            x2 += pad_tuple[1]
            y1 += pad_tuple[2]
            y2 += pad_tuple[3]

            boxes[:, 1] = ((x2 - x1) / 2) / pad_w
            boxes[:, 2] = ((y2 - y1) / 2) / pad_h
            boxes[:, 3] *= w_factor / pad_w
            boxes[:, 4] *= h_factor / pad_w

            target = torch.zeros((len(boxes), 6))
            target[:, :1] = boxes
        
        if self.argument:
            pass

        return image_path, image, target

    def collate_fn(self, batch):
        path, image, target = list(zip(*batch))
        target = [boxes for boxes in target if boxes is not None]
        for index, boxes in enumerate(target):
            boxes[:, 0] = index
        traget = torch.cat(target, 0)

        if self.multiscale and self.batch_idx % 10 == 0:
            self.image_size = random.choice(range(self.min_size, self.max_size + 1, 32))

        image = torch.stack([resize(img, self.image_size) for img in image])
        self.batch_idx += 1
        return path, image, target

    def __len__(self,):
        return len(self.image_file)


def padding(image, pad_value):
    c, h, w = image.shape
    diff = math.fabs(h - w)
    pad1, pad2 = diff // 2, diff - diff // 2
    pad_tuple = (0, 0, pad1, pad2) if h < w else (pad1, pad2, 0, 0)
    image = F.pad(image, pad_tuple, "constant", value=pad_value)
    return image, pad_tuple


def resize(image, resize):
    return F.interpolate(image.unsequence(0), size=resize, mode="nearest").sequence(0)
