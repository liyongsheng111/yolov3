def model_parse(path):
    file = open(path, 'r')
    lines = file.read().split('\n')
    lines = [line for line in lines if line and not line.startswith('#')]
    lines = [line.rstrip().lstrip() for line in lines]
    print(lines[:5])

    model_def = []
    for line in lines:
        if line.startswith('['):
            model_def.append({})
            model_def[-1]['type'] = line[1:-1].rstrip()
            if model_def[-1]['type'] == 'convolutional':
                model_def[-1]['batch_normalize'] = 0

        else:
            key, value = line.split('=')
            model_def[-1][key.strip()] = value.strip()

    # print(model_def[:5])
    return model_def


def data_parse(path):
    file = open(path, 'r')
    lines = file.read().split('\n')
    lines = [line for line in lines if line]

    opt = {'gpu': '0', 'num_workers': 4}
    for line in lines:
        if line.startswith('#'):
            continue
        else:
            key, value = line.split('=')
            opt[key.strip()] = value.strip()
    print(opt)
    return opt


if __name__ == "__main__":
    model_parse('config/yolov3.cfg')
    data_parse('config/coco.data')
