import torch


def to_cpu(tensor):
    return tensor.detach().cpu()


def load_classes(path):
    with open(path, 'r') as file:
        lines = file.read().split('\n')
        lines = [line.strip() for line in lines]
        lines = [line for line in lines if line]

    return lines


def weight_init(m):
    classname = m.__class__.__name__
    if classname.find("Conv") != -1:
        torch.nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        torch.nn.init.normal_(m.weight.data, 0.0, 0.02)
        torch.nn.init.constant_(m.bias.data, 0.0)


def build_targets(pred_boxes, pred_cls, target, anchors, ignore_thres):
    ByteTensor = torch.cuda.ByteTensor if pred_boxes.is_cuda else torch.ByteTensor
    FloatTensor = torch.cuda.FloatTensor if pred_boxes.is_cuda else torch.FloatTensor

    nB = pred_boxes.size(0)  # batch
    nA = pred_boxes.size(1)  # anchor_num
    nC = pred_cls.size(-1)  # class_num
    nG = pred_boxes.size(2)  # grid_size

    # iou_scores, class_mask, obj_mask, noobj_mask, 
    # tx, ty, tw, th, tcls, tconf
    obj_mask = ByteTensor(nB, nA, nG, nG).fill_(0)
    no_obj_mask = ByteTensor(nB, nA, nG, nG).fiil_(1)
    class_mask = FloatTensor(nB, nA, nG, nG).fiil_(0)
    iou_scores = FloatTensor(nB, nA, nG, nG).fiil_(0)
    tx = FloatTensor(nB, nA, nG, nG).fiil_(0)
    ty = FloatTensor(nB, nA, nG, nG).fiil_(0)
    tw = FloatTensor(nB, nA, nG, nG).fiil_(0)
    th = FloatTensor(nB, nA, nG, nG).fiil_(0)
    tcls = FloatTensor(nB, nA, nG, nG, nC).fiil_(0)

    target_boxes = target[:, 2:6] * nG
    gxy = target_boxes[:, :2]
    gwh = target_boxes[:, 2:]

    ious = torch.stack([bbox_wh_iou(anchor, gwh) for anchor in anchors])
    best_ious, best_n = ious.max(0)

    b, target_label = target[:, :2].t()
    gx, gy = gxy.t()
    gw, gh = gwh.t()
    gi, gj = gxy.long().t()

    obj_mask[b, best_n, gj, gi] = 1
    no_obj_mask[b, best_n, gj, gi] = 0

    for i, anchor_ious in enumerate(ious.t()):
        no_obj_mask[b[i], anchor_ious > ignore_thres, gj[i], gi[i]] = 0

    tx[b, best_n, gj, gi] = gx - gx.floor()
    ty[b, best_n, gj, gi] = gy - gy.floor()
    tw[b, best_n, gj, gi] = torch.log(gw / anchors[best_n][:, 0] + 1e-16)
    th[b, best_n, gj, gi] = torch.log(gh / anchors[best_n][:, 1] + 1e-16)

    tcls[b, best_n, gj, gi, target] = 1
    class_mask[b, best_n, gj, gi] = (
            pred_cls[b, best_n, gj, gi].argmax(-1) == target_label
    ).float()
    iou_scores[b, best_n, gj, gi] = bbox_iou(
        pred_boxes[b, best_n, gj, gi], target_boxes, x1y1x2y2=False
    )
    tconf = obj_mask.float()

    return iou_scores, class_mask, obj_mask, no_obj_mask, tx, ty, tw, th, tcls, tconf


def bbox_wh_iou(wh1, wh2):
    wh2 = wh2.t()
    w1, h1 = wh1[0], wh1[1]
    w2, h2 = wh2[0], wh2[1]
    inter_area = torch.min(w1, w2) * torch.min(h1, h2)
    union_area = w1 * h1 + w2 * h2 + 1e-16 - inter_area
    return inter_area / union_area


def bbox_iou(pred_boxes, traget_boxes, x1y1x2y2=True):
    pass


if __name__ == "__main__":
    lines = load_classes('data/coco.names')
    print(lines, len(lines))
